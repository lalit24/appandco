
import { NotificationsService, NotificationType } from 'angular2-notifications';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/shared/auth.service';

import { Component,  ViewChild, Output, EventEmitter } from '@angular/core';
import { NgForm } from '@angular/forms';
import { WizardComponent as ArcWizardComponent } from 'angular-archwizard';

@Component({
  selector: 'setup-account-password',
  templateUrl: './setup-account.component.html',
})
export class SetupAccountComponent {

  //@Output() postFinalize = new EventEmitter();
  @ViewChild('formStep1') formStep1: NgForm;
  @ViewChild('formStep2') formStep2: NgForm;
  @ViewChild('formStep3') formStep3: NgForm;
  @ViewChild('formStep4') formStep4: NgForm;
  @ViewChild('wizard') wizard: ArcWizardComponent;
  //@ViewChild('setupAccountForm') setupAccountForm: NgForm;
  Name = "";
  Date = new Date();
  Number = 1000;
  PreviewAddress = "";
  PreviewPhone="";
  PreviewTax ="";

  buttonDisabled = false;
  buttonState = '';
  posting = false;
  isCollapsedBusiness = false;
  isCollapsedPhone = false;
  isCollapsedTax = false;

  constructor(
    private authService: AuthService,
    private notifications: NotificationsService,
    private router: Router
  ) { }
  
  onNextStep1(): void {
    debugger;
    this.formStep1.onSubmit(null);
    
    if (this.formStep1.valid) {
      this.buttonDisabled = true;
      this.buttonState = 'show-spinner';
      this.buttonDisabled = false;
      this.buttonState = '';
      this.Name = this.formStep1.value.name;
      //this.valueChanged();

      this.notifications.create(
        'Done',
        'Step 1 Compleated',
        NotificationType.Bare,
        { theClass: 'outline primary', timeOut: 1000, showProgressBar: true }
      );
      this.wizard.goToNextStep();
    }
  }

  onNextStep2(): void {
    debugger;
    this.formStep2.onSubmit(null);
    if (this.formStep2.valid) {
      this.notifications.create(
        'Done',
        'Step 2 Compleated',
        NotificationType.Bare,
        { theClass: 'outline primary', timeOut: 1000, showProgressBar: true }
      ); 
      this.wizard.goToNextStep();
      this.formStep3.value.form3name = this.Name;
      this.formStep3.value.currency = ["INR"];
      this.formStep3.value.format = ["English(India)"];
      this.formStep3.value.zone = ["(GMT+05:30) Chennai, Kolkata, Mumbai, New Delhi"];
    }
  }

  valueChanged(){
    alert('hello');
  }

  onNextStep3(): void {
    debugger;
    this.formStep3.onSubmit(null);
    if (this.formStep3.valid) {
      this.notifications.create(
        'Done',
        'Step 3 Compleated',
        NotificationType.Bare,
        { theClass: 'outline primary', timeOut: 1000, showProgressBar: true }
      );
      this.wizard.goToNextStep();
    }
  }

  onNextStep4(): void {
    debugger;
    this.formStep4.onSubmit(null);
    if (this.formStep4.valid) {
      this.posting = true;
      setTimeout(() => {
        this.posting = false;
      }, 2000);
      this.wizard.goToNextStep();
    }
  }

  setaddres(){
    debugger;
    this.PreviewAddress = this.formStep4.value.street;

    if(this.formStep4.value.apt){
      if(this.PreviewAddress){
        this.PreviewAddress = this.PreviewAddress +","+ this.formStep4.value.apt;
      }else{
        this.PreviewAddress = this.formStep4.value.apt;
      }
    }

    if(this.formStep4.value.city){
      if(this.PreviewAddress){
        this.PreviewAddress = this.PreviewAddress +","+ this.formStep4.value.city;
      }else{
        this.PreviewAddress = this.formStep4.value.city;
      } 
    }

    if(this.formStep4.value.zip){
      if(this.PreviewAddress){
        this.PreviewAddress = this.PreviewAddress +","+ this.formStep4.value.zip;
      }else{
        this.PreviewAddress = this.formStep4.value.zip;
      }
    }

    if(this.formStep4.value.country){
      if(this.PreviewAddress){
        this.PreviewAddress = this.PreviewAddress +","+ this.formStep4.value.country;
      }else{
        this.PreviewAddress = this.formStep4.value.country;
      }
    }

    if(this.formStep4.value.state){
      if(this.PreviewAddress){
        this.PreviewAddress = this.PreviewAddress +","+ this.formStep4.value.state;
      }else{
        this.PreviewAddress = this.formStep4.value.state;
      }
    }

    this.PreviewPhone = this.formStep4.value.phone;

    this.PreviewTax = this.formStep4.value.lebel; 
    if(this.formStep4.value.idnumber){
      if(this.PreviewTax){
        this.PreviewTax = this.PreviewTax + "," +this.formStep4.value.idnumber;
      }else{
        this.PreviewTax = this.formStep4.value.idnumber;
      }
    }
  }
  

  // onSubmit(): void {
  //   debugger;
  //   if (this.setupAccountForm.valid && !this.buttonDisabled) {
  //     this.buttonDisabled = true;
  //     this.buttonState = 'show-spinner';

  //     this.buttonDisabled = false;
  //     this.buttonState = '';

  //     this.notifications.create(
  //       'Done',
  //       'Step 1 Compleated',
  //       NotificationType.Bare,
  //       { theClass: 'outline primary', timeOut: 1000, showProgressBar: true }
  //     );

  //     // this.authService
  //     //   .resetPassword(this.setupAccountForm.value)
  //     //   .then((data) => {
  //     //     debugger;
  //     //     this.notifications.create(
  //     //       'Done',
  //     //       'Password reset completed, you will be redirected to Login page!',
  //     //       NotificationType.Bare,
  //     //       { theClass: 'outline primary', timeOut: 6000, showProgressBar: true }
  //     //     );
  //     //     this.buttonDisabled = false;
  //     //     this.buttonState = '';
  //     //     setTimeout(() => {
  //     //       this.router.navigate(['user/login']);
  //     //     }, 6000);
  //     //   })
  //     //   .catch((error) => {
  //     //     debugger;
  //     //     this.buttonDisabled = false;
  //     //     this.buttonState = '';
  //     //     this.notifications.create(
  //     //       'Error',
  //     //       error.message,
  //     //       NotificationType.Bare,
  //     //       { theClass: 'outline primary', timeOut: 6000, showProgressBar: false }
  //     //     );
  //     //   });
  //   }else{
  //         this.notifications.create(
  //           'Error',
  //           "Not Valid",
  //           NotificationType.Warn,
  //           { theClass: 'outline primary', timeOut: 1000, showProgressBar: false }
  //         );
  //   }
  // }
}
