import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { HotkeysService, Hotkey } from 'angular2-hotkeys';
import { ApiService, IInventory, Inventory } from 'src/app/data/api.service';
import { NotificationsService, NotificationType } from 'angular2-notifications';
import { ContextMenuComponent } from 'ngx-contextmenu';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { NgForm } from '@angular/forms';
import { AddNewInventoryModalComponent } from 'src/app/containers/pages/add-new-inventory-model/add-new-inventory-model.component';

@Component({
    selector: 'app-inventory-list',
    templateUrl: './inventory-list.component.html'
  })

  export class InventoryListComponent implements OnInit {
    form: NgForm;
    modalRef: BsModalRef;
    displayMode = 'list';
    selectAllState = '';
    selected: IInventory[] = [];
    currentPage = 1;
    itemsPerPage = 10;
    search = '';
    orderBy = '';
    isLoading: boolean;
    endOfTheList = false;
    totalItem = 0;
    totalPage = 0;
  
    @ViewChild('basicMenu') public basicMenu: ContextMenuComponent;
    @ViewChild('addNewModalRef', { static: true }) addNewModalRef: AddNewInventoryModalComponent;
  
    constructor(private hotkeysService: HotkeysService, public apiService: ApiService, private notifications: NotificationsService,
                private modalService: BsModalService) {
      this.hotkeysService.add(new Hotkey('ctrl+a', (event: KeyboardEvent): boolean => {
        this.selected = [...this.apiService.inventorydata];
        return false;
      }));
      this.hotkeysService.add(new Hotkey('ctrl+d', (event: KeyboardEvent): boolean => {
        this.selected = [];
        return false;
      }));
    }
  
  
    ngOnInit(): void {
      debugger;
      this.apiService.inventorydata= [];
      this.apiService.inventorymodel = new Inventory(Inventory);
      //this.loadData(this.itemsPerPage, this.currentPage, this.search, this.orderBy);
    }
    
    changeDisplayMode(mode): void {
      this.displayMode = mode;
    }
  
    showAddNewModal(): void {
      debugger;
      this.apiService.inventorymodel = new Inventory(Inventory);
      this.apiService.inventorymodel.name = undefined;
      this.apiService.inventorymodel.taxtype = "C";
      this.addNewModalRef.show();
    }

    isSelected(p: IInventory): boolean {
      return this.selected.findIndex(x => x.id === p.id) > -1;
    }
  
    onSelect(item: IInventory): void {
      if (this.isSelected(item)) {
        this.selected = this.selected.filter(x => x.id !== item.id);
      } else {
        this.selected.push(item);
      }
      this.setSelectAllState();
    }

    setSelectAllState(): void {
      if (this.selected.length === this.apiService.inventorydata.length) {
        this.selectAllState = 'checked';
      } else if (this.selected.length !== 0) {
        this.selectAllState = 'indeterminate';
      } else {
        this.selectAllState = '';
      }
    }
  
    selectAllChange($event): void {
      if ($event.target.checked) {
        this.selected = [...this.apiService.inventorydata];
      } else {
        this.selected = [];
      }
      this.setSelectAllState();
    }

    pageChanged(event: any): void {
      //this.loadData(this.itemsPerPage, event.page, this.search, this.orderBy);
    }
  
    itemsPerPageChange(perPage: number): void {
      //this.loadData(perPage, 1, this.search, this.orderBy);
    }
  
    changeOrderBy(item: any): void {
      //this.loadData(this.itemsPerPage, 1, this.search, item.value);
    }
  
    searchKeyUp(event): void {
      const val = event.target.value.toLowerCase().trim();
      //this.loadData(this.itemsPerPage, 1, val, this.orderBy);
    }

    onContextMenuClick(action: string, item: IInventory): void {
      debugger;
      if(action == "delete"){
        if(item.id){
          this.apiService.inventorydata.splice(this.apiService.inventorydata.findIndex((row) => row.id === item.id), 1);
          var i = 0;
          this.apiService.inventorydata.forEach(c => c.id = ++i);
          this.notifications.create(
            'Done',
            'Delete Successfully',
            NotificationType.Bare,
            { theClass: 'outline primary', timeOut: 1000, showProgressBar: true }
            );
        //console.log('onContextMenuClick -> action :  ', action, ', item.title :', item.title);
        }else {
          this.notifications.create(
            'Error',
            "Id not found",
            NotificationType.Warn,
            { theClass: 'outline primary', timeOut: 1000, showProgressBar: false }
          );
        }
      }
    }
    
    openModal(template: TemplateRef<any>): void {
      this.modalRef = this.modalService.show(template, { class: 'modal-sm' });
    }
  
    viewItem(p: IInventory){
      debugger;
      this.apiService.inventorymodel = new Inventory(Inventory);
      this.apiService.inventorymodel = p;
      this.addNewModalRef.show();
    }
}