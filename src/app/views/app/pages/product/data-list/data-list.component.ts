import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { AddNewProductModalComponent } from 'src/app/containers/pages/add-new-product-modal/add-new-product-modal.component';
import { HotkeysService, Hotkey } from 'angular2-hotkeys';
import { ApiService, Customer, ICustomer } from 'src/app/data/api.service';
import { IProduct } from 'src/app/data/api.service';
import { NotificationsService, NotificationType } from 'angular2-notifications';
import { ContextMenuComponent } from 'ngx-contextmenu';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-data-list',
  templateUrl: './data-list.component.html'
})
export class DataListComponent implements OnInit {
  form: NgForm;
  modalRef: BsModalRef;
  displayMode = 'list';
  selectAllState = '';
  // selected: IProduct[] = [];
  // data: IProduct[] = [];
  selected: ICustomer[] = [];
  //data: ICustomer[] = [];
  currentPage = 1;
  itemsPerPage = 10;
  search = '';
  orderBy = '';
  isLoading: boolean;
  endOfTheList = false;
  totalItem = 0;
  totalPage = 0;

  @ViewChild('basicMenu') public basicMenu: ContextMenuComponent;
  @ViewChild('addNewModalRef', { static: true }) addNewModalRef: AddNewProductModalComponent;

  constructor(private hotkeysService: HotkeysService, public apiService: ApiService, private notifications: NotificationsService,
              private modalService: BsModalService) {
    this.hotkeysService.add(new Hotkey('ctrl+a', (event: KeyboardEvent): boolean => {
      this.selected = [...this.apiService.customerdata];
      return false;
    }));
    this.hotkeysService.add(new Hotkey('ctrl+d', (event: KeyboardEvent): boolean => {
      this.selected = [];
      return false;
    }));
  }


  ngOnInit(): void {
    debugger;
    this.apiService.customerdata= [];
    this.apiService.model = new Customer(Customer);
    this.loadData(this.itemsPerPage, this.currentPage, this.search, this.orderBy);
  }

  loadData(pageSize: number = 10, currentPage: number = 1, search: string = '', orderBy: string = ''): void {
    this.itemsPerPage = pageSize;
    this.currentPage = currentPage;
    this.search = search;
    this.orderBy = orderBy;
    //this.apiService.customerdata.push({id: 1, title: "Client",name : "Test Client",lastemailsent : "-",totalproject: 0, status: "ACTIVE",statusColor: "secondary",income:0})
    // this.apiService.getProducts(pageSize, currentPage, search, orderBy).subscribe(
    //   data => {
    //     debugger;
    //     if (data.status) {
    //       this.isLoading = false;
    //       this.data = data.data.map(x => {
    //         return {
    //           ...x,
    //           img: x.img.replace('/img/', '/img/products/')
    //         };
    //       });
    //       this.totalItem = data.totalItem;
    //       this.totalPage = data.totalPage;
    //       this.setSelectAllState();
    //     } else {
    //       this.endOfTheList = true;
    //     }
    //   },
    //   error => {
    //     this.isLoading = false;
    //   }
    // );
  }

  changeDisplayMode(mode): void {
    this.displayMode = mode;
  }

  showAddNewModal(): void {
    debugger;
    this.apiService.model = new Customer(Customer);
    this.apiService.model.name = undefined;
    this.apiService.model.taxtype = "C";
    this.addNewModalRef.show();
  }

  // isSelected(p: IProduct): boolean {
  isSelected(p: ICustomer): boolean {
    return this.selected.findIndex(x => x.id === p.id) > -1;
  }

  //onSelect(item: IProduct): void {
  onSelect(item: ICustomer): void {
    if (this.isSelected(item)) {
      this.selected = this.selected.filter(x => x.id !== item.id);
    } else {
      this.selected.push(item);
    }
    this.setSelectAllState();
  }

  setSelectAllState(): void {
    if (this.selected.length === this.apiService.customerdata.length) {
      this.selectAllState = 'checked';
    } else if (this.selected.length !== 0) {
      this.selectAllState = 'indeterminate';
    } else {
      this.selectAllState = '';
    }
  }

  selectAllChange($event): void {
    if ($event.target.checked) {
      this.selected = [...this.apiService.customerdata];
    } else {
      this.selected = [];
    }
    this.setSelectAllState();
  }

  pageChanged(event: any): void {
    this.loadData(this.itemsPerPage, event.page, this.search, this.orderBy);
  }

  itemsPerPageChange(perPage: number): void {
    this.loadData(perPage, 1, this.search, this.orderBy);
  }

  changeOrderBy(item: any): void {
    this.loadData(this.itemsPerPage, 1, this.search, item.value);
  }

  searchKeyUp(event): void {
    const val = event.target.value.toLowerCase().trim();
    this.loadData(this.itemsPerPage, 1, val, this.orderBy);
  }

  //onContextMenuClick(action: string, item: IProduct): void {
  onContextMenuClick(action: string, item: ICustomer): void {
    debugger;
    //this.openModal(template)
    if(action == "delete"){
      if(item.id){
        this.apiService.customerdata.splice(this.apiService.customerdata.findIndex((row) => row.id === item.id), 1);
        var i = 0;
        this.apiService.customerdata.forEach(c => c.id = ++i);
        this.notifications.create(
          'Done',
          'Delete Successfully',
          NotificationType.Bare,
          { theClass: 'outline primary', timeOut: 1000, showProgressBar: true }
          );
      //console.log('onContextMenuClick -> action :  ', action, ', item.title :', item.title);
      }else {
        this.notifications.create(
          'Error',
          "Id not found",
          NotificationType.Warn,
          { theClass: 'outline primary', timeOut: 1000, showProgressBar: false }
        );
      }
    }
  }
  
  openModal(template: TemplateRef<any>): void {
    this.modalRef = this.modalService.show(template, { class: 'modal-sm' });
  }

  viewItem(p: ICustomer){
    debugger;
    this.apiService.model = new Customer(Customer);
    this.apiService.model = p;
    this.addNewModalRef.show();
  }
}
