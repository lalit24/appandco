import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddNewProductModalComponent } from 'src/app/containers/pages/add-new-product-modal/add-new-product-modal.component';
import { ClientListComponent } from './client-list/client-list.component';



@NgModule({
  declarations: [ClientListComponent],
  imports: [
    CommonModule
  ]
})
export class PagesModule { }
