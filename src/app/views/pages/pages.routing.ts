import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ClientListComponent } from './client-list/client-list.component';
import { PagesComponent } from './pages.component';

const routes: Routes = [
    {
        path: '', component: PagesComponent,
        children: [
            //{ path: '', redirectTo: 'login', pathMatch: 'full' },
            { path: 'data-list', component: ClientListComponent },
            //{ path: 'register', component: RegisterComponent },
            //{ path: 'forgot-password', component: ForgotPasswordComponent },
            //{ path: 'reset-password', component: ResetPasswordComponent },
            //{ path: 'setup-account', component: SetupAccountComponent}
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class PagesRoutingModule { }
