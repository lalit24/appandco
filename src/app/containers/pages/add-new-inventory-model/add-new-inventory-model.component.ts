import { Component, TemplateRef, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { NotificationsService, NotificationType } from 'angular2-notifications';
import { ApiService, Inventory } from 'src/app/data/api.service';
@Component({
    selector: 'app-add-new-inventory-model',
    templateUrl: './add-new-inventory-model.component.html',
    styles: []
})

 export class AddNewInventoryModalComponent {
    modalRef: BsModalRef;
    config = {
        backdrop: true,
        ignoreBackdropClick: true,
        class: 'modal-right'
    };

    @ViewChild('template', { static: true }) template: TemplateRef<any>;
    @ViewChild('form') form: NgForm;

    constructor(private modalService: BsModalService, private notifications: NotificationsService,
        public apiService: ApiService) { }

    show(): void {
        this.modalRef = this.modalService.show(this.template, this.config);
    }

    onSubmit(form) {
        debugger;
        var Id = 0;
        if (form.valid) {
          if(this.apiService.inventorymodel.id){
            form.form.value.id = this.apiService.inventorymodel.id;
            form.form.value.totalproject = this.apiService.inventorymodel.totalproject;
            form.form.value.lastemailsent = this.apiService.inventorymodel.lastemailsent;
            form.form.value.income = this.apiService.inventorymodel.income;
            form.form.value.status = this.apiService.inventorymodel.status;
            form.form.value.statusColor = this.apiService.inventorymodel.statusColor;
          }
          if(!form.form.value.id){
            if(this.apiService.inventorydata && this.apiService.inventorydata.length > 0){
              Id = this.apiService.inventorydata.length;
            }
            form.form.value.id = Number(Id) + 1;
            form.form.value.totalproject = 0;
            form.form.value.lastemailsent = "-";
            form.form.value.income = 0;
            form.form.value.status = "ACTIVE"
            form.form.value.statusColor = "secondary"
            this.apiService.inventorydata.push(form.form.value)
            this.notifications.create(
              'Done',
              'Saved Successfully',
              NotificationType.Bare,
              { theClass: 'outline primary', timeOut: 1000, showProgressBar: true }
            );
          }else{
            const index = this.apiService.inventorydata.findIndex((existinglocation) => existinglocation.id === existinglocation.id);
            this.apiService.inventorydata[index] = new Inventory(form.form.value);
            this.notifications.create(
              'Done',
              'Updated Successfully',
              NotificationType.Bare,
              { theClass: 'outline primary', timeOut: 1000, showProgressBar: true }
            );
          }
          this.modalRef.hide()
        } else {
          this.notifications.create(
            'Error',
            "Please Enter mandatory fileds",
            NotificationType.Warn,
            { theClass: 'outline primary', timeOut: 1000, showProgressBar: false }
          );
        }
      }
}