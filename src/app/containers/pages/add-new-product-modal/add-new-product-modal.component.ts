import { Component, TemplateRef, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { NotificationsService, NotificationType } from 'angular2-notifications';
import { ApiService, Customer } from 'src/app/data/api.service';
@Component({
  selector: 'app-add-new-product-modal',
  templateUrl: './add-new-product-modal.component.html',
  styles: []
})
export class AddNewProductModalComponent {
  modalRef: BsModalRef;
  config = {
    backdrop: true,
    ignoreBackdropClick: true,
    class: 'modal-right'
  };
  country = [
    { label: 'India', value: 'India' }
  ];


  @ViewChild('template', { static: true }) template: TemplateRef<any>;
  @ViewChild('form') form: NgForm;

  constructor(private modalService: BsModalService, private notifications: NotificationsService,
              public apiService: ApiService) { }

  ngOnInit(){
    debugger;
    //this.form.form.value.name.setValue("Vipul");
  }
  ngAfterViewInit(){
    //alert('aa')
  }
  show(): void {
    this.modalRef = this.modalService.show(this.template, this.config);
  }

  onSubmit(form) {
    debugger;
    var Id = 0;
    if (form.valid) {
      if(this.apiService.model.id){
        form.form.value.id = this.apiService.model.id;
        form.form.value.totalproject = this.apiService.model.totalproject;
        form.form.value.lastemailsent = this.apiService.model.lastemailsent;
        form.form.value.income = this.apiService.model.income;
        form.form.value.status = this.apiService.model.status;
        form.form.value.statusColor = this.apiService.model.statusColor;
      }
      if(!form.form.value.id){
        if(this.apiService.customerdata && this.apiService.customerdata.length > 0){
          Id = this.apiService.customerdata.length;
        }
        form.form.value.id = Number(Id) + 1;
        form.form.value.totalproject = 0;
        form.form.value.lastemailsent = "-";
        form.form.value.income = 0;
        form.form.value.status = "ACTIVE"
        form.form.value.statusColor = "secondary"
        this.apiService.customerdata.push(form.form.value)
        this.notifications.create(
          'Done',
          'Saved Successfully',
          NotificationType.Bare,
          { theClass: 'outline primary', timeOut: 1000, showProgressBar: true }
        );
      }else{
        const index = this.apiService.customerdata.findIndex((existinglocation) => existinglocation.id === existinglocation.id);
        this.apiService.customerdata[index] = new Customer(form.form.value);
        this.notifications.create(
          'Done',
          'Updated Successfully',
          NotificationType.Bare,
          { theClass: 'outline primary', timeOut: 1000, showProgressBar: true }
        );
      }
      this.modalRef.hide()
    } else {
      this.notifications.create(
        'Error',
        "Please Enter mandatory fileds",
        NotificationType.Warn,
        { theClass: 'outline primary', timeOut: 1000, showProgressBar: false }
      );
    }
  }

}
