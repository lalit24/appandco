import { Injectable } from '@angular/core';
import {
  HttpClient,
  HttpParams
} from '@angular/common/http';
import { map, catchError, tap } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { environment } from 'src/environments/environment';

export interface IProduct {
  id: number;
  title: string;
  img: string;
  category: string;
  status: string;
  statusColor: string;
  description: string;
  sales: number;
  stock: number;
  date: string;
}

export interface ICustomer {
  id: any;
  title: any;
  name: any;
  contact: any;
  email: any;
  address: any;
  taxtype : any;
  lable : any;
  idnumber : any;
  notes : any;
  tags : any;
  
  city: any;
  state: any;
  street: any;
  suite: any;
  zipcode: any;
  lastemailsent: any;
  totalproject: any;
  status: any;
  statusColor: any;
  income: any;
}

export interface IInventory {
  id: any;
  title: any;
  name: any;
  contact: any;
  email: any;
  address: any;
  taxtype : any;
  lable : any;
  idnumber : any;
  notes : any;
  tags : any;
  lastemailsent: any;
  totalproject: any;
  status: any;
  statusColor: any;
  income: any;
}
export class Customer {
  id            : any;
  title         : any;
  name          : any;
  contact       : any;
  email         : any;
  address       : any;
  taxtype       : any;
  lable         : any;
  idnumber      : any;
  notes          : any;
  tags          : any;

  city          : any;
  state         : any;
  street        : any;
  suite         : any;
  zipcode       : any;
  lastemailsent : any;
  totalproject  : any;
  status        : any;
  statusColor   : any;
  income        : any;
  constructor(Customer){
    this.id             =  Customer.id;
    this.title          =  Customer.title;
    this.name           =  Customer.name;
    this.contact        =  Customer.contact;
    this.email          =  Customer.email;
    this.address        =  Customer.address;
    this.taxtype        =  Customer.taxtype;
    this.lable          =  Customer.lable;
    this.idnumber       =  Customer.idnumber;
    this.notes          =  Customer.notes;
    this.tags           =  Customer.tags;

    this.city           =  Customer.city;
    this.state          =  Customer.state;
    this.street         =  Customer.street;
    this.suite          =  Customer.suite;
    this.zipcode        =  Customer.zipcode;
    this.lastemailsent  =  Customer.lastemailsent;
    this.totalproject   =  Customer.totalproject;
    this.status         =  Customer.status;
    this.statusColor    =  Customer.statusColor;
    this.income         =  Customer.income;
  }
}

export class Inventory {
  id            : any;
  title         : any;
  name          : any;
  contact       : any;
  email         : any;
  address       : any;
  taxtype       : any;
  lable         : any;
  idnumber      : any;
  notes          : any;
  tags          : any;
  lastemailsent : any;
  totalproject  : any;
  status        : any;
  statusColor   : any;
  income        : any;
  constructor(Inventory){
    this.id             =  Inventory.id;
    this.title          =  Inventory.title;
    this.name           =  Inventory.name;
    this.contact        =  Inventory.contact;
    this.email          =  Inventory.email;
    this.address        =  Inventory.address;
    this.taxtype        =  Inventory.taxtype;
    this.lable          =  Inventory.lable;
    this.idnumber       =  Inventory.idnumber;
    this.notes          =  Inventory.notes;
    this.tags           =  Inventory.tags;
    this.lastemailsent  =  Inventory.lastemailsent;
    this.totalproject   =  Inventory.totalproject;
    this.status         =  Inventory.status;
    this.statusColor    =  Inventory.statusColor;
    this.income         =  Inventory.income;
  }
}

export interface IProductResponse {
  data: IProduct[];
  status: boolean;
  totalItem: number;
  totalPage: number;
  pageSize: string;
  currentPage: string;
}

@Injectable({ providedIn: 'root' })
export class ApiService {
  constructor(private http: HttpClient) { }
  customerdata: ICustomer[] = [];
  model = new Customer(Customer);

  inventorydata: IInventory[] = [];
  inventorymodel = new Inventory(Inventory);
  
  // tslint:disable-next-line:typedef
  getProducts(pageSize: number = 10, currentPage: number = 1, search: string = '', orderBy: string = '') {
    const url = environment.apiUrl + '/cakes/paging';
    let params = new HttpParams();
    params = params.append('pageSize', pageSize + '');
    params = params.append('currentPage', currentPage + '');
    params = params.append('search', search);
    params = params.append('orderBy', orderBy);

    return this.http.get(url, { params })
      .pipe(
        map((res: IProductResponse) => {
          return res;
        }),
        catchError(errorRes => {
          return throwError(errorRes);
        })
      );
  }
}
